/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.inheritance;

/**
 *
 * @author Satit Wapeetao
 */
public class Testanimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);       
        animal.walk();
        animal.speak();
        
        Dog dang = new Dog ("Dang","Black&White");
        dang.speak();
        dang.walk();
        
        Dog To = new Dog ("To","Black&White");
        To.speak();
        To.walk();
        
        Dog Mome = new Dog ("Mome","White&Black");
        Mome.speak();
        Mome.walk();
        
        Dog Bat = new Dog ("Bat","White&Black");
        Bat.speak();
        Bat.walk();
        
        Cat zero = new Cat("zero","Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("zom","orsnge");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck gabgab = new Duck("gabgab","orange");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println();
        System.out.println("Animal is Dog "+(animal instanceof Dog));
        System.out.println("Animal is Cat "+(animal instanceof Cat));
        System.out.println("Animal is Duck "+(animal instanceof Duck));
        System.out.println("Animal is Animal "+(animal instanceof Animal));
        System.out.println("dang is Dog "+(dang instanceof Dog));
        System.out.println("dang is Animal "+(dang instanceof Animal));
        System.out.println("To is Dog "+(To instanceof Dog));
        System.out.println("To is Animal "+(To instanceof Animal));
        System.out.println("Mome is Dog "+(Mome instanceof Dog));
        System.out.println("Mome is Animal "+(Mome instanceof Animal));
        System.out.println("Bat is Dog "+(Bat instanceof Dog));
        System.out.println("Bat is Animal "+(Bat instanceof Animal));
        System.out.println("zero is Cat "+(zero instanceof Cat));
        System.out.println("zero is Animal "+(zero instanceof Animal));
        System.out.println("zom is Duck "+(zom instanceof Duck));
        System.out.println("zom is Animal "+(zom instanceof Animal));
        System.out.println("gabgab is Duck "+(gabgab instanceof Duck));
        System.out.println("gabgab is Animal "+(gabgab instanceof Animal));
        System.out.println();
        
        
        Animal[] animals = {dang,To,Mome,Bat,zero,zom,gabgab};
        for(int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
        
    }
}
