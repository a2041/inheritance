/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.inheritance;

import java.awt.BorderLayout;

/**
 *
 * @author Satit Wapeetao
 */
public class Animal {
    protected String name;
    public int numberofLegs=0;
    private String color;
    public Animal(String name,String color,int numberofLegs){
        System.out.println("Animal created");
        this.name=name;
        this.color=color;
        this.numberofLegs= numberofLegs;
    }
    public void walk(){
        System.out.println("Animal walk");
    }
    public void speak(){
        System.out.println("Animal speak");
        System.out.println("name: "+this.name+" color: "+this.color
                +" numberOflegs: "+this.numberofLegs);
    }

    public String getName() {
        return name;
    }

    public int getNumberofLegs() {
        return numberofLegs;
    }

    public String getColor() {
        return color;
    }
    
}
